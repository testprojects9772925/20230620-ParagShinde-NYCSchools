import Foundation

class School: Decodable {

    var name: String
    var email: String?
    var dbn: String
    var city: String
    
    enum CodingKeys: String, CodingKey {
        case school_name
        case school_email
        case dbn
        case city
    }

    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = try container.decode(String.self, forKey: .school_name)
        self.dbn = try container.decode(String.self, forKey: .dbn)
        self.city = try container.decode(String.self, forKey: .city)
        do {
            self.email = try container.decode(String.self, forKey: .school_email)
        } catch _ as NSError {
            self.email = nil
        }
    }
}
