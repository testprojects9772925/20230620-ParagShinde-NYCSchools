import Foundation

class SATResult: Decodable {

    var schoolName: String
    var dbn: String
    var numberOfSATTakers: String
    var avgCriticalReadingScore: String
    var avgMathScore: String
    var avgWritingScore: String
    
    enum CodingKeys: String, CodingKey {
        case school_name
        case dbn
        case num_of_sat_test_takers
        case sat_critical_reading_avg_score
        case sat_math_avg_score
        case sat_writing_avg_score
    }

    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.schoolName = try container.decode(String.self, forKey: .school_name)
        self.dbn = try container.decode(String.self, forKey: .dbn)
        self.numberOfSATTakers = try container.decode(String.self, forKey: .num_of_sat_test_takers)
        self.avgCriticalReadingScore = try container.decode(String.self, forKey: .sat_critical_reading_avg_score)
        self.avgMathScore = try container.decode(String.self, forKey: .sat_math_avg_score)
        self.avgWritingScore = try container.decode(String.self, forKey: .sat_writing_avg_score)
    }
}
