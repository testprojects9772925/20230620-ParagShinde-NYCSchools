import Foundation
import Alamofire

enum NetworkError: Error {
    case noInternetConnection
    case requestTimeout
    case invalidResponse
}

struct APIClient {
    
    static func getAllSchooldData(completion: @escaping (Result<[School], Error>) -> Void) {
        AF.request("https://data.cityofnewyork.us/resource/s3k6-pzi2.json").responseJSON { response in
            switch response.result {
            case .success(_):
                do {
                    let schools = try JSONDecoder().decode([School].self, from: response.data!)
                    completion(Result.success(schools))
                } catch let error as NSError {
                    completion(Result.failure(error))
                }
            case .failure(let error):
                completion(Result.failure(error))
            }
        }
    }
    
    static func getSATResult(forSchool dbn: String, completion: @escaping (Result<SATResult, Error>) -> Void) {
        let requestString = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn=" + dbn
        
        AF.request(requestString).responseJSON { response in
            switch response.result {
            case .success(_):
                do {
                    let satResult = try JSONDecoder().decode([SATResult].self, from: response.data!)
                    if let firstResult = satResult.first {
                        completion(Result.success(firstResult))
                    } else {
                        completion(Result.failure(NetworkError.invalidResponse))
                    }
                } catch let error as NSError {
                    completion(Result.failure(error))
                }
            case .failure(let error):
                completion(Result.failure(error))
            }
        }
    }
}
