import UIKit

class ViewController: UIViewController {

    var schools: [School] = []
    
    @IBOutlet weak var errorView: UIView!
    @IBOutlet weak var loadingSpinner: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: VC Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "NYC Schools"
        self.loadData()
    }
    
    // MARK: Data Processing
    private func loadData() {
        APIClient.getAllSchooldData{ result in
            switch result {
            case .success(let schools):
                self.schools = schools.sorted { $0.name < $1.name }
                self.reloadTableView()
            case .failure(_):
                self.showErrorView()
            }
        }
    }
    
    @IBAction func tryAgainButtonTapped(_ sender: Any) {
        self.loadingSpinner.startAnimating()
        self.errorView.isHidden = true
        self.loadData()
    }
    
    // MARK: View Handling
    private func showErrorView() {
        self.loadingSpinner.stopAnimating()
        self.errorView.isHidden = false
    }
    
    private func reloadTableView() {
        self.loadingSpinner.stopAnimating()
        self.errorView.isHidden = true
        self.tableView.isHidden = false
        self.tableView.reloadData()
    }
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.schools.count;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let school = self.schools[indexPath.row]
        let schooldDetailsVC = SchoolDetailsViewController(school: school)
        self.navigationController?.pushViewController(schooldDetailsVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "schoolCell")
        let school = self.schools[indexPath.row]
        cell?.textLabel?.text = school.name
        cell?.detailTextLabel?.text = school.city
        return cell!
    }
    
}
