import UIKit

class SchoolDetailsViewController: UIViewController {
    
    @IBOutlet weak var loadingSpinner: UIActivityIndicatorView!
    @IBOutlet weak var firstPartDetails: UILabel!
    @IBOutlet weak var numberOfSATTakerLabel: UILabel!
    @IBOutlet weak var readingScoreLabel: UILabel!
    @IBOutlet weak var mathScoreLabel: UILabel!
    @IBOutlet weak var writingScoreLabel: UILabel!
    @IBOutlet weak var satScoreLabel: UILabel!
    
    @IBOutlet weak var errorView: UIView!
    
    let school: School
    
    // MARK: Init
    public init(school: School) {
        self.school = school
        super.init(nibName: "SchoolDetailsViewController", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: VC Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadingSpinner.startAnimating()
        self.loadSATResult()
    }
    
    // MARK: Private Helpers
    private func loadSATResult() {
        APIClient.getSATResult(forSchool: self.school.dbn) { result in
            switch result {
            case .success(let satResult):
                self.updateUI(forSatResult: satResult)
            case .failure(let error):
                self.errorView.isHidden = false
                print("Error:\(error)")
            }
            self.loadingSpinner.stopAnimating()
        }
    }
    
    @IBAction func tryAgainButtonTapped(_ sender: Any) {
        self.loadingSpinner.startAnimating()
        self.loadSATResult()
        self.errorView.isHidden = true
    }
    
    private func updateUI(forSatResult result:SATResult) {
        self.errorView.isHidden = true
        let schoolDetails = self.school.name + "\n" + self.school.city
        firstPartDetails.text = schoolDetails
        self.firstPartDetails.isHidden = false
        self.numberOfSATTakerLabel.text = "Numbers of student taken SAT test: " + result.numberOfSATTakers
        self.numberOfSATTakerLabel.isHidden = false
        self.mathScoreLabel.text = "Math score: " + result.avgMathScore
        self.mathScoreLabel.isHidden = false
        self.writingScoreLabel.text = "Wriring score: " + result.avgWritingScore
        self.writingScoreLabel.isHidden = false
        self.readingScoreLabel.text = "Reading score: " + result.avgCriticalReadingScore
        self.readingScoreLabel.isHidden = false
        self.satScoreLabel.isHidden = false
    }

}
