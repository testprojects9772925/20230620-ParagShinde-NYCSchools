import XCTest
@testable import NYCSchool

final class NYCSchoolTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(NYCSchool().text, "Hello, World!")
    }
}
